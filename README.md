# Game-ID-DB

Scripts to provide the SQL needed for the id -> name resolving on Pixel FX products (`gameid.php`).

Also contains a PHP script (`gameid-cover.php`) to deliver cover art (PS1 and PS2)

## Checkout

Use `git clone --recursive` (to get all submodules) in order to run the `create-dbs` script.

## PS1

The id to name database was originally created by Thanos from 8bitmods and later refined by Subelement.

PS1 also has a gameid to canonical id stage for multi-disc titles.
