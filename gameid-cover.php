<?php

error_reporting(E_ALL);
ini_set('ignore_repeated_errors', TRUE);
ini_set('display_errors', FALSE);
ini_set('log_errors', TRUE);
ini_set('error_log', '___ERROR_LOG___');

$mysql_pass = @file_get_contents("___MYSQL_KEYFILE___");
if ($mysql_pass === FALSE) {
    http_response_code(404);
    die("not found Y\n");
}

$base_path = "___PATH_TO_COVERS___";

$pid = @preg_replace('/[^A-Za-z0-9_\-]/', '', @$_REQUEST['pid']);
$gameid = @preg_replace('/[\/]/', '', @$_REQUEST['gameid']);
$tablename = FALSE;

if (strtolower($pid) == "ps1" || (int) $pid == 0x01 || hexdec($pid) == 0x01) {
    $pid = "psx-covers/covers/default/";
    $tablename = "ps1digital";
} else if (strtolower($pid) == "ps2" || (int) $pid == 0x02 || hexdec($pid) == 0x02) {
    $pid = "ps2-covers/covers/default/";
    $tablename = "ps2digital";
} else if (strtolower($pid) == "n64" || (int) $pid == 0x03 || hexdec($pid) == 0x03) {
    $pid = "n64-covers/covers/default/";
    $tablename = "n64digital";
} else if (strtolower($pid) == "xbox" || (int) $pid == 0x04 || hexdec($pid) == 0x04) {
    $pid = "xbox-xbd/";
    $tablename = "xboxdigital";
} else {
    $pid = "";
}

try {
    $file = $base_path . "no_image.png";

    // remap id to canonical id
    header("X-Game-ID-Orig: " . $gameid);
    if ($tablename !== FALSE) {
        $mysqli = @new mysqli("___MYSQL_HOST___", "___MYSQL_USER___", $mysql_pass, "gameid", ___MYSQL_PORT___);
        if (!mysqli_connect_error()) {
            try {
                $table = $mysqli->real_escape_string($tablename);
                $query = 'SELECT name, setId FROM '.$table.' WHERE gameId=?';
                $result = $mysqli->execute_query($query, [$gameid]);
                foreach ($result as $row) {
                    $gameid = $row["setId"];
                    $gamename = $row["name"];
                    $gamename = preg_replace('/"/', "\\\"", $gamename);
                    header("X-Game-ID-Canon: " . $gameid);
                    header("X-Game-ID-Name: \"" . $gamename . "\"\n");
                    break;
                }
            } catch (Exception $err) {}
        }
    }

    if ($pid !== false) {
        $files_to_test = [
            // Fallback per console, then global
            $base_path . $pid . "no_image.png",
            $base_path . "no_image.png",
        ];

        // Special case for Xbox
        if ($pid === "xbox-xbd/") {
            $matched = preg_match('/([a-zA-Z]{2})-(\d{3})$/', $gameid, $matches);

            if ($matched) {
                array_unshift(
                    $files_to_test,
                    $base_path . $pid . "titles/" . strtoupper($matches[1]) . "/" . $matches[2] . "/cover_front.jpg"
                );
            }
        }
        // All other consoles
        else {
            array_unshift(
                $files_to_test,
                $base_path . $pid . $gameid . ".png",
                $base_path . $pid . $gameid . ".jpg",
                $base_path . $pid . strtoupper($gameid) . ".png",
                $base_path . $pid . strtoupper($gameid) . ".jpg"
            );
        }

        foreach ($files_to_test as $i => $value) {
            if (file_exists($value)) {
                $file = $value;
                break;
            }
        }
    }

    $data = file_get_contents($file);
    $mtype = mime_content_type($file);

    header("Content-Type: " . $mtype);
    header("Content-Length: " . strlen($data));
    echo $data;
} catch (Exception $err) {}
