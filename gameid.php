<?php

error_reporting(E_ALL);
ini_set('ignore_repeated_errors', TRUE);
ini_set('display_errors', FALSE);
ini_set('log_errors', TRUE);
ini_set('error_log', '___ERROR_LOG___');

$mysql_pass = @file_get_contents("___MYSQL_KEYFILE___");
if ($mysql_pass === FALSE) {
    http_response_code(404);
    die("not found Y\n");
}
$pid = @preg_replace('/[^A-Za-z0-9_\-]/', '', @$_REQUEST['pid']);
if (!$pid) {
    http_response_code(400);
    die("pid missing.\n");
}
$gameid = @preg_replace('/[\/]/', '', @$_REQUEST['gameid']);
if (!$gameid) {
    http_response_code(400);
    die("gameid missing.\n");
}
$n = @preg_replace('/[\/]/', '', @$_REQUEST['n']);

$mysqli = @new mysqli("___MYSQL_HOST___", "___MYSQL_USER___", $mysql_pass, "gameid", ___MYSQL_PORT___);

if (mysqli_connect_error()) {
    http_response_code(500);
    die("D-ERROR\n");
}

$response = "";
$gameid_q = $gameid;

if (strtolower($pid) == "ps1" || (int) $pid == 0x01 || hexdec($pid) == 0x01) {
    $pid = "ps1digital";
} else if (strtolower($pid) == "ps2" || (int) $pid == 0x02 || hexdec($pid) == 0x02) {
    $pid = "ps2digital";
} else if (strtolower($pid) == "n64" || (int) $pid == 0x03 || hexdec($pid) == 0x03) {
    $pid = "n64digital";
} else if (strtolower($pid) == "xbox" || (int) $pid == 0x04 || hexdec($pid) == 0x04) {
    $pid = "xboxdigital";
} else if (strtolower($pid) == "gc" || (int) $pid == 0x06 || hexdec($pid) == 0x06) {
    $pid = "gcdigital";
    #$gameid_q = preg_replace('/([0-9a-fA-F]{16}).*/m', "$1", $gameid);
}

$response = $gameid;
$found = false;

try {
    $table = $mysqli->real_escape_string($pid);
    $query = 'SELECT name, setId FROM '.$table.' WHERE gameId=?';
    $result = $mysqli->execute_query($query, [$gameid_q]);
    foreach ($result as $row) {
        $canonid = $row["setId"];
        $name = $row["name"];
        if ($n >= 1) {
            $response = $canonid . "\t" . $name;
        } else {
            $response = $name;
        }
        $found = true;
        break;
    }
} catch (Exception $err) {}

header("Content-Type: text/plain");
header("Content-Length: " . strlen($response));
header("X-Gameid-Found: " . ($found ? "true" : "false"));
echo $response;
